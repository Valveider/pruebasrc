package conexbd;

import java.sql.*;

public class ConexBD {
    Connection con = null;
 //comentario
    //comentario2
/**
* Método utilizado para establecer la conexión con la base de datos
* @return devuelve la conexión
*/
public Connection crearConex()
{
   try {
      String driver = "com.mysql.jdbc.Driver";
      Class.forName(driver);
      
      con = DriverManager.getConnection("jdbc:mysql://localhost/alumnos","root","admin");
   } catch (SQLException | ClassNotFoundException ex) {
       System.out.println("Error: " + ex);
   }
 
   return con;
}
 
/**
*
*Método utilizado para realizar las instrucciones: INSERT, DELETE y UPDATE
*@param sql Cadena que contiene la instrucción SQL a ejecutar
*@return estado regresa el estado de la ejecución, true(éxito) o false(error)
*
*/
public boolean ejecutarSQL(String sql)
{
   try {
      con = crearConex();
      Statement sentencia = (Statement) con.createStatement();
      sentencia.executeUpdate(sql);
   } catch (SQLException ex) {
       System.out.println("Error: " + ex);
   }
 
   return true;
}
 
/**
*
*Método utilizado para realizar la instrucción SELECT
*@param sql Cadena que contiene la instrucción SQL a ejecutar
*@return resultado regresa los registros generados por la consulta
*
*/
public ResultSet ejecutarSQLSelect(String sql)
{
   ResultSet resultado = null;
   try {
       con = crearConex();
      Statement sentencia = con.createStatement();
      resultado = sentencia.executeQuery(sql);
   } catch (SQLException ex) {
      System.out.println("Error: " + ex);
   }
 
   return resultado;
}
}
